#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

dest_shard="${OVERRIDDEN_DEST_SHARD:-$(cat dest-shard.txt)}"
slice_length=$((TOTAL_PROJECTS/CI_NODE_TOTAL))
slice_end=$((CI_NODE_INDEX*slice_length))

for project_data in $(cat projects-data.json | head -n "${slice_end}" | tail -n "${slice_length}"); do
  project_id=$(echo "${project_data}" | jq -r '.id')
  namespace_id=$(echo "${project_data}" | jq -r '.namespace')

  plan=$(curl "https://${GITLAB_HOST}/api/v4/namespaces/${namespace_id}" \
    --request GET \
    --header "Private-Token: ${GITLAB_ADMIN_TOKEN}" \
    --silent | jq -r '.plan')

  if [[ "${plan}" != "free" && "${plan}" != "null" ]]; then
    echo "Project #${project_id} is not free or null (got ${plan}), removing its custom attribute and moving along ..."

    curl "https://${GITLAB_HOST}/api/v4/projects/${project_id}/custom_attributes/${CUSTOM_ATTR_KEY}" \
      --request DELETE \
      --header "Private-Token: ${GITLAB_ADMIN_TOKEN}" \
      --silent \
      --output /dev/null

    continue
  fi

  echo "Scheduling a move for project #${project_id}"

  code=$(curl "https://${GITLAB_HOST}/api/v4/projects/${project_id}/repository_storage_moves" \
    --request POST \
    --header "Content-Type: application/json" \
    --header "Private-Token: ${GITLAB_ADMIN_TOKEN}" \
    --data "{\"destination_storage_name\": \"${dest_shard}\"}" \
    --output /dev/null \
    --silent \
    --write-out "%{http_code}")

  if [[ "${code}" != "201" ]]; then
    echo "Scheduling a move for project #${project_id} failed, got response ${code}, skipping ..."
    continue
  fi

  echo "Setting custom attribute for project #${project_id} to scheduled"

  curl "https://${GITLAB_HOST}/api/v4/projects/${project_id}/custom_attributes/${CUSTOM_ATTR_KEY}" \
    --request PUT \
    --header "Private-Token: ${GITLAB_ADMIN_TOKEN}" \
    --silent \
    --output /dev/null \
    --data "value=scheduled"
done
