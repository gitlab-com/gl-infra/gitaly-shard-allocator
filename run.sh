#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

PROJECT_ID=gitlab-production
DATASET_ID=gitaly_shard_selector
LOCATION=us
SCHEMA="activity_date:DATE,repository_id:STRING,request_count:INTEGER"
EXTERNAL_DEF_FILE="$(mktemp)"
TABLE_NAME="${PROJECT_ID}:${DATASET_ID}.repo_usage"

date=${1:-}

# Check if the date is a valid date, or print usage
[[ "${date}" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]] || {
	cat <<-EOF
		Usage: $0 YYYY-MM-DD
		----------------------------
		Imports data from Gitaly logging bucket on GCS into BigQuery table
		($TABLE_NAME) for the provided date.
	EOF
	exit 1
}

# Parse date
Y=$(echo "$date" | cut -d- -f1)
M=$(echo "$date" | cut -d- -f2)
D=$(echo "$date" | cut -d- -f3)

echo "Importing Gitaly GCS log data for ${Y}-${M}-${D}..."

cat <<-EOF >"${EXTERNAL_DEF_FILE}"
	{
		"maxBadRecords": 5,
		"ignoreUnknownValues": true,
		"autodetect": true,
		"csvOptions": {
			"encoding": "UTF-8",
			"quote": "\"",
			"fieldDelimiter": "§",
			"allowJaggedRows": true
		},
		"schema": {
			"fields": [
				{
					"name": "json",
					"type": "STRING"
				}
			]
		},
		"sourceFormat": "CSV",
		"sourceUris": [
			"gs://gitlab-gprd-logging-archive/gitaly/${Y}/${M}/${D}/*"
		]
	}
EOF

function biq_query_create_partition() {
	bq \
		--location "${LOCATION}" \
		--project_id "${PROJECT_ID}" \
		--dataset_id "${DATASET_ID}" \
		query \
		--external_table_definition="gitaly_logs::${EXTERNAL_DEF_FILE}" \
		--nouse_legacy_sql \
		--destination_table "${PROJECT_ID}:${DATASET_ID}.repo_usage" \
		--time_partitioning_field activity_date \
		--time_partitioning_type DAY \
		--destination_schema "${SCHEMA}" \
		--clustering_fields repository_id \
		--allow_large_results \
		--apilog stderr \
		--append_table
}

cat <<-EOF | biq_query_create_partition
	SELECT *
		FROM (
			SELECT DATE(${Y}, ${M}, ${D}) activity_date,
						JSON_EXTRACT_SCALAR(gitaly_logs.json, "$.jsonPayload['grpc.request.glRepository']") repository_id,
						COUNT(*) request_count
				FROM gitaly_logs
			WHERE gitaly_logs.json LIKE '%grpc.code%'
			GROUP BY repository_id
		)
		WHERE repository_id IS NOT NULL AND repository_id <> ""
EOF
