#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

total_projects=${TOTAL_PROJECTS:-300}
projects_per_page=${PROJECTS_PER_PAGE:-100}
url="https://${GITLAB_HOST}/api/v4/projects?custom_attributes\[${CUSTOM_ATTR_KEY}\]=${CUSTOM_ATTR_VALUE}&per_page=${projects_per_page}&pagination=keyset&order_by=id"
iterations=$((total_projects/projects_per_page))

for i in $(seq 1 $iterations); do
  curl "${url}" \
    --header "Private-Token: ${GITLAB_ADMIN_TOKEN}" \
    --dump-header headers.txt \
    --output projects.json
  cat projects.json | jq -c '.[] | {id: .id, namespace: .namespace.id}' >> projects-data.json

  url=$(cat headers.txt | grep -Ei '^Link:' | sed 's/.*<\(.*\)>; rel="next".*/\1/' || true)

  # We reached the end of the projects set early, so break the loop
  if [[ -z "${url}" ]]; then
    break
  fi
done

if [[ ! -s projects-data.json ]]; then
  echo "No projects were selected! Failing."
  exit 1
fi
