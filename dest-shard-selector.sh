#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

query="sort_desc(instance:node_filesystem_avail:ratio{env='${ENVIRONMENT:-gstg}',type='gitaly',device='/dev/sdb',shard='hdd',monitor='default'} > ${DEST_SHARD_CUTOFF:-0.20})"

shard=$(curl "http://thanos-query-01-inf-ops.c.gitlab-ops.internal:10902/api/v1/query" \
  -G \
  --data-urlencode "query=${query}" \
  --data-urlencode "time=$(date -u +%s).0" |
  jq -r '.data.result[0].metric.fqdn' |
  sed 's/file-hdd-\([[:digit:]]*\).*/nfs-file-hdd\1/')

if [[ -z "${shard}" || "${shard}" = "null" ]]; then
  echo "No destination shard was selected! Failing."
  exit 1
fi

echo "${shard}" > dest-shard.txt
